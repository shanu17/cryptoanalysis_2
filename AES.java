import java.security.Key;
import java.security.Security;
import java.util.Scanner;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.util.encoders.Hex;






public class AES {

	 public static void main(
		        String[]    args)
		        throws Exception
		    {
		 		//Security.getProvider("BC");
		 		System.out.println("Enter the string to encrypt using AES: ");
		 		Scanner keyboard = new Scanner(System.in);
		 		String s_input = keyboard.nextLine();
		 		byte[] input = s_input.getBytes();
		        byte[]		    ivBytes = new byte[] { 
		                            0x00, 0x00, 0x00, 0x01, 0x04, 0x05, 0x06, 0x07,
		                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01 };
		        
		        Cipher          cipher = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
		        KeyGenerator    generator = KeyGenerator.getInstance("AES", "BC");

		        generator.init(256);
		        
		        Key encryptionKey = generator.generateKey();
		        String hex = Hex.toHexString(encryptionKey.getEncoded());
		        System.out.println("key   : " + hex);
		        
		        // encryption pass
		        
		        cipher.init(Cipher.ENCRYPT_MODE, encryptionKey, new IvParameterSpec(ivBytes));
		        
		        byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
		        
		        int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
		        
		        ctLength += cipher.doFinal(cipherText, ctLength);
		        
		        String c_text = new String(cipherText);
		        System.out.println("The ciphertext is : " + c_text);
		        
		        // decryption pass
		        
		        Key	decryptionKey = new SecretKeySpec(encryptionKey.getEncoded(), encryptionKey.getAlgorithm());
		        
		        cipher.init(Cipher.DECRYPT_MODE, decryptionKey, new IvParameterSpec(ivBytes));
		        
		        byte[] plainText = new byte[cipher.getOutputSize(ctLength)];

		        int ptLength = cipher.update(cipherText, 0, ctLength, plainText, 0);
		        
		        ptLength += cipher.doFinal(plainText, ptLength);
		        
		        String plain = new String(plainText);
		        
		        System.out.println("The decrypted cipher text is  :  " + plain);
		    }

}
