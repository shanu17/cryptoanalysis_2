import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.util.Scanner;
import javax.crypto.Cipher;
import java.security.Signature;



public class RSA {
    public static SecureRandom createFixedRandom()
    {
        return new SecureRandom();
    }
	public static void main(
	        String[]    args)
	        throws Exception
	    {
			System.out.println("Enter the string to encrypt using RSA: ");
			Scanner keyboard = new Scanner(System.in);
			String s_input = keyboard.nextLine();
			byte[] input = s_input.getBytes();
	        Cipher	         cipher = Cipher.getInstance("RSA/NONE/NoPadding", "BC");
	        SecureRandom     random = createFixedRandom();
	        
	        // create the keys
	        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");
	        Signature signature = Signature.getInstance("SHA1withRSA", "BC");
	        
	        generator.initialize(2048, random);

	        KeyPair          pair = generator.generateKeyPair();
	        Key              pubKey = pair.getPublic();
	        Key              privKey = pair.getPrivate();

	        // generate a signature
	        signature.initSign(pair.getPrivate(), createFixedRandom());
	        
	        signature.update(input);
	        
	        byte[]  sigBytes = signature.sign();
	        
	        // encryption step
	        
	        cipher.init(Cipher.ENCRYPT_MODE, pubKey, random);

	        byte[] cipherText = cipher.doFinal(input);

	        String c_text = new String(cipherText);
	        System.out.println("The ciphertext is : " + c_text);
	        
	        // decryption step

	        cipher.init(Cipher.DECRYPT_MODE, privKey);

	        byte[] plainText = cipher.doFinal(cipherText);
	        
	        String plain = new String(plainText);
	        
	        System.out.println("The decrypted cipher text is  :  " + plain);
	        
	        signature.initVerify(pair.getPublic());
	        
	        signature.update(plainText);
	        
	        if (signature.verify(sigBytes))
	        {
	            System.out.println("Signature verification succeeded.");
	        }
	        else
	        {
	            System.out.println("Signature verification failed.");
	        }
	    }

}
