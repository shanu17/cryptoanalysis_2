import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;

public class extra_credit {
	static String getAlphaNumericString(int n) 
    { 
  
        // chose a Character random from this String 
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"+ "0123456789"+ "abcdefghijklmnopqrstuvxyz"; 
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index = (int)(AlphaNumericString.length() * Math.random()); 
  
            // add Character one by one in end of sb 
            sb.append(AlphaNumericString .charAt(index)); 
        } 
  
        return sb.toString(); 
    } 
    public static SecureRandom createFixedRandom()
    {
        return new SecureRandom();
    }
	
	public static void main(String[] args) 
    { 
  
        String rand_string[] = new String[100];
        for (int i = 0; i<100; i++)
        {
        	rand_string[i]=getAlphaNumericString(100);
        }
        
        
        
        
        byte[] ivBytes = new byte[] { 0x00, 0x00, 0x00, 0x01, 0x04, 0x05, 0x06, 0x07,0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01 };

        for(int j=0;j<2;j++)
        {
        try {
			
        	long startTime_aes = System.nanoTime();  // Start AES time
        	
        	Cipher cipher_aes = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");    // AES cipher
			KeyGenerator generator_aes = KeyGenerator.getInstance("AES", "BC");
			generator_aes.init(256);
			Key encryptionKey_aes = generator_aes.generateKey();                     // AES Key gen
			cipher_aes.init(Cipher.ENCRYPT_MODE, encryptionKey_aes, new IvParameterSpec(ivBytes));
			
			for(int i = 0;i<100 ; i++)
			{
				byte[] input = rand_string[i].getBytes();
				byte[] cipherText = new byte[cipher_aes.getOutputSize(input.length)];
				int ctLength = cipher_aes.update(input, 0, input.length, cipherText, 0);
				ctLength += cipher_aes.doFinal(cipherText, ctLength);
			}
			long endTime_aes = System.nanoTime();
			
			long duration_aes = (endTime_aes - startTime_aes);
			//System.out.println(duration_aes);
			
			long startTime_rsa = System.nanoTime(); //Start time for rsa
			
			Cipher cipher_rsa = Cipher.getInstance("RSA/NONE/NoPadding", "BC");
			KeyPairGenerator generator_rsa = KeyPairGenerator.getInstance("RSA", "BC");
			SecureRandom     random = createFixedRandom();
			generator_rsa.initialize(2048, random);
	        KeyPair pair_rsa = generator_rsa.generateKeyPair();
	        Key pubKey_rsa = pair_rsa.getPublic();
	        
	        cipher_rsa.init(Cipher.ENCRYPT_MODE, pubKey_rsa, random);
	        
	        for(int i=0;i<100;i++)
	        {
	        	byte[] input = rand_string[i].getBytes();
	        	byte[] cipherText = cipher_rsa.doFinal(input);
	        }
	        
	        long endTime_rsa = System.nanoTime();   //End time for rsa
	        long duration_rsa = (endTime_rsa - startTime_rsa);
	        //System.out.println(duration_rsa);
	        
	        long startTime_2fish = System.nanoTime();
	        
	        Cipher cipher_2fish = Cipher.getInstance("Twofish/CBC/PKCS7Padding", "BC");	        
	        KeyGenerator    generator_2fish = KeyGenerator.getInstance("Twofish", "BC");
	        Key encryptionKey_2fish = generator_2fish.generateKey();                  //TWOFISH KEY GEN
	        cipher_2fish.init(Cipher.ENCRYPT_MODE, encryptionKey_2fish, new IvParameterSpec(ivBytes));
	        
	        for(int i =0;i<100;i++)
	        {
	        	byte[] input = rand_string[i].getBytes();
	        	byte[] cipherText = new byte[cipher_2fish.getOutputSize(input.length)];
	        	int ctLength = cipher_2fish.update(input, 0, input.length, cipherText, 0);	        
	        	ctLength += cipher_2fish.doFinal(cipherText, ctLength);
	        }
	        
	        long endTime_2fish = System.nanoTime();
	        long duration_2fish = (endTime_2fish - startTime_2fish);
	        //System.out.println(duration_2fish);
	        
	        if(j==1)
	        {
	        	System.out.print("AES is faster than RSA by (%) :  ");
	        	System.out.println((duration_rsa - duration_aes)/duration_aes);
	        	System.out.print("Twofish is faster than RSA by (%) :  ");
	        	System.out.println(((duration_rsa - duration_2fish)/(duration_2fish)));
	        	if(duration_aes > duration_2fish)
	        	{
	        		System.out.print("Twofish is faster than AES by (%) :  ");
	        		System.out.println((duration_aes - duration_2fish)/duration_2fish);
	        	}
	        	else
	        	{
	        		System.out.print("AES is faster than Twofish by (%) :  ");
	        		System.out.println((duration_2fish-duration_aes)/duration_aes);
	        	}
	        }
		} catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException | ShortBufferException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		}
        
        }
        
        
    } 
}
